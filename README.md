# Birthday wishes

Sends birthday wishes via Gmail API to your friends if their birthdays are stored as Google Calendar events. **You must have Gmail and Google Calendar accounts to use this.**

Each birthday event must include the word `birthday` (or `Geburtstag` or `gimtadienis`) and the `email` of the birthday person. The event name can also include the language code for the wishes. Currently has birthday wishes in English, German and Lithuanian with language codes `EN`, `DE` and `LT`, respectively.

## Set up:
### (a) To simply run once:

1. Go to https://developers.google.com/calendar/quickstart/python, keep the default selection 'Desktop app' and push 'Enable the Google Calendar API'. In the resulting dialog click DOWNLOAD CLIENT CONFIGURATION and save the file `credentials.json` to your working directory.

2. Go to https://developers.google.com/gmail/api/quickstart/python, keep the default selection 'Desktop app' and push 'Enable the Gmail API'. DO NOT download the resulting `credentials.json` file. It will get updated on Step 4.

3. Run `pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib` on the terminal.

4. Download the file `birthdayWishes.py`. Then go to the directory of this file and run, e.g. from the terminal on Linux or Mac, `birthdayWishes.py`.

5. Running for the first time will prompt the Google client authentication screen. Choose your client. When you see the screen `"This app isn't verified"` click `Advanced` and `Go to Quickstart(unsafe)` (`Quickstart` is the name of the Google Calendar project to which we granted permission in Step 1.) Grant for `Quickstart` permissions to `1. View events on all your calendars`, `2. View your calendars` and `3. Send email on your behalf`. 

6. The program might hang afterwards. If that happens, stop it and run it again. Authentication should have been saved in `token.pickle` file and you should not be asked for it. If you are, then go through authentication again.

7. The program continues running until turned off, checking for the day's birthdays everyday at 00:01 local machine time.

## Troubleshooting (a)
- If the program says `"Today's wishes are already sent."`, but they're not, then delete the contents of the file `ranToday`.

- If nothing's happening, just stop the program and run it again.

- Sometimes Google's servers just time out and nothing happens. In this case just run it again.

- Sending Gmail emails falls under "Sensitive" authorisation scope and to put in production such desktop apps requires verification by Google which at the time of writing costs $15000 – $75000. More information: 

https://support.google.com/cloud/answer/7454865?hl=en \
https://developers.google.com/gmail/api/auth/scopes \
https://support.google.com/cloud/answer/9110914?hl=en


### (b) To run at startup:
#### MacOS

1. Download the `startup.command` file and put it into the same directory as `birthdayWishes.py`. Change the entry `yourDirectory` to the directory containing `birthdayWishes.py` file. If you're using a package manager, then activate it in the `startup.command` file (the file contains anaconda as an example). Make the file executable e.g. by running `chmod +x startup.command` once on the terminal.

2. Add `startup.command` file to your startup under `System Preferences > Users and Groups > Login Items`.

Tested on MacOS 10.14.5.

#### Ubuntu

1. Download the `startup.sh` file and put it into the same directory as `birthdayWishes.py`. Change the entry `yourDirectory` to the directory containing `birthdayWishes.py` file. If you're using a package manager, then activate it in the `startup.sh` file (the file contains anaconda as an example). Make the file executable e.g. by running `chmod +x startup.sh` once on the terminal.

2. Add `startup.sh` file to your startup by going `Startup Applications Preferences`, pressing `Add` and putting `/yourdirectory/startup.sh` under the `Command` tab.

Tested on Ubuntu 18.04.

## Troubleshooting (b)
- The program starts with a 60 second delay to make sure that everything is booted and that the internet is connected. One way to check if the program ran successfully is to check if the file `ranToday` contains today's date after some time. The program continues running and checks for birthdays again at 00:01 local machine time. If `ranToday` is empty, then add the line `exec >> outlog` to your `startup.sh` (Ubuntu) or `startup.command` (Mac) file. This will print the output to `outlog` file (with some delay) for debugging.

- Even if everything is set up correctly, the Google's servers might still fail. See `Troubleshooting (a)` above for more details.

- If you're using a virtual environment, i.e. if you've installed the Google Python API client in Step (a3) in a virtual environment, then modify the file `startup.sh` (Ubuntu) or `startup.command` (Mac) accordingly. The files contain the code for anaconda as an example. Failure to do so will most probably result in failing to import `googleapiclient.discovery`.
