from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from email.mime.text import MIMEText
import base64

import re

import schedule
import time
from pytz import timezone


################################ some global variables ################################
#######################################################################################
# If modifying these scopes, delete the file token.pickle.
SCOPES = [ 'https://www.googleapis.com/auth/gmail.send',
           'https://www.googleapis.com/auth/calendar.readonly',
           'https://www.googleapis.com/auth/calendar.events.readonly' ]

################################ different languages; modify as necessary ############
LANGS = 'EN|DE|LT'

WISHES = {
          'EN': ['Happy birthday!', 'Wish you all the best! Have a great party!'],
          'DE': ['Herzlichen Glueckwunsh!', 'Alles gute zum Geburtstag!'],
          'LT': ['Sveikinimai!', 'Sveikinu su gimtadieniu!']
}

KEYWORDS = {
             'EN': ['birthday'],
             'DE': ['Geburtstag', 'geburtstag'],
             'LT': ['gimtadienis']
}
########################################################################################


def createWishes(sender, to, languageCode):
  """Create a message for an email.

  Args:
    sender: Email address of the sender.
    to: Email address of the receiver.
    languageCode: The language of the wish 'EN', 'DE' or 'LT'.

  Returns:
    An object containing a base64url encoded email object.
  """
  message = MIMEText(WISHES[languageCode][1])
  message['to'] = to
  message['from'] = sender
  message['subject'] = WISHES[languageCode][0]
  return {'raw': base64.urlsafe_b64encode(message.as_string().encode()).decode()}


def sendWishes(service, user_id, message):
  """Send an email birthday wish.

  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    message: Message to be sent.

  Returns:
    Sent message.
  """
  try:
    message = (service.users().messages().send(userId=user_id, body=message)
               .execute())
    return message
  except errors.HttpError:
    print('An error occurred: %s' % error)


def checkCredentials():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run.
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return creds


def main():
    """ Send birthday wishes to the people mentioned in calendar events.

        Goes over the events of the day. For each event checks if it contains one
        of the birthday KEYWORDS. If it does and the email address is also provided
        in the calendar event description, then sends an email with one of the WISHES.
        If language is specified in the event, then sends a wish from WISHES
        in the corresponding language. If not, then sends the English wish from
        WISHES. Keeps a log if the wishes for the day have already been sent
        inside the file ranToday.

    Returns:
      0 if successful.
    """
    now = datetime.datetime.utcnow().isoformat()

    # The file ranToday stores if the program has successfully run today. It is
    # created automatically when the program runs for the first time.
    if os.path.exists('ranToday'):
        with open('ranToday', 'r+') as log:
            date = log.read()[:10]
            # If wishes already sent, then stop.
            if date == now[:10]:
                print('Today\'s wishes are already sent.')
                return 0

    # Create the file for the next run. Write to file at the end, if successful.
    else:
        with open('ranToday', 'wb'): pass

    creds = checkCredentials()

    serviceCal   = build('calendar', 'v3', credentials=creds)
    serviceGmail = build('gmail', 'v1', credentials=creds)

    # Get the time zone of the main calendar.
    primaryCal     = serviceCal.calendars().get(calendarId='primary').execute()
    timeZone       = primaryCal.get('timeZone')
    timeZoneOffset = str(datetime.datetime.now(timezone(timeZone)))[-6:]

    todayStart = now[:10] + 'T' + '00:00:00' + timeZoneOffset
    todayEnd   = now[:10] + 'T' + '23:59:59' + timeZoneOffset

    eventsResult = serviceCal.events().list(calendarId='primary', timeMin=todayStart,
                                        timeMax=todayEnd, singleEvents=True,
                                        orderBy='startTime').execute()
    events = eventsResult.get('items', [])

    if not events:
        print('No birthdays found today.')

    birthdayKeywords = list(KEYWORDS.values())
    for event in events:
        eventString = event['summary']

        # Check if the event contains one of the birthday keywords.
        for keyword in birthdayKeywords:
            if keyword[0] in eventString:

                # Try reading off the email address to send wishes to.
                try:
                    receiversEmail = re.findall('\S+@\S+', eventString)[0]
                except IndexError:
                    print('No email address found in the event\'s "'\
                            + eventString + '" description! No wishes sent in this case.')
                    break

                # Try reading off the wish language from the event.
                try:
                    wishLanguage   = re.findall(LANGS,  eventString)[0]

                # If no language given, set to English as the default.
                except IndexError:
                    wishLanguage = 'EN'

                wish = createWishes('me', receiversEmail, languageCode = wishLanguage)
                sendWishes(serviceGmail, 'me', wish)
                print('Wishes corresponding to the event "' + eventString + '" sent.')

    with open('ranToday', 'r+') as log:
      # Record having sent today's birthday wishes.
      log.write(now[:10])

    return 0


# Run main once each time the script is run; ideally should be run at each startup.
main()

# Schedule the function main to be run every day. This is necessary if the computer
# runs for more than one day and the script is only run on startup.
schedule.every().day.at("00:01").do(main)
print('The program continues running; next check for the upcoming day\'s'
      + ' birthdays at 00:01.')

while True:
    schedule.run_pending()
    time.sleep(60) # wait one minute
