#!/bin/bash
sleep 60 		# allow time for booting and connecting to the internet

# uncomment below if using anaconda
### make sure that 'conda activate yourEnvironment' is well-defined at startup
#if [ -f "/pathToYourAnaconda/anaconda3/etc/profile.d/conda.sh" ]; then
#    . "/pathToYourAnaconda/anaconda3/etc/profile.d/conda.sh"
#    CONDA_CHANGEPS1=false conda activate yourEnvironment
#fi
#conda activate yourEnvironment

cd /yourDirectory/
python /yourDirectory/birthdayWishes.py
